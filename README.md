
# Proving the Proofs

A smart contract, when making meaningful decisions based on data passed in by an Oracle, has to be able to trust the data.  When the data is widely publicly available, oracles like Chainlink are able to create trusted data by sourcing the data from many independent actors thereby creating distributed trust in the data in the same way blockchains create distributed trust.

When the data is only available through a single trusted source, as is the case for private asset data, on Inveniam.io another trust mechanism must be available.

Such a trust mechanism is provided by Accumulate which provides a mechanism for data to be provided to a smart contract, via an oracle, and for the smart contract to then verify the data against anchors previously written into the smart contract’s host blockchain.

In the following we will explain how an Accumulate Digital Identity (ADI) along with a set of merkle roots can be accepted from an oracle into a smart contract and then how the smart contract can then cryptographically verify the ADI against an anchor entry in the smart contract host blockchain.

Using the described method the smart contract authors can insure the data they have received is in fact that data sent from the Accumulate blockchain and, in this case, verify the identity of the authorizer of the distribution.

Even if a bad actor gets in between the data source and the oracle or even comprises the oracle infrastructure, the resulting cryptographic checks will not match allowing the smart contract to reject altered data.

The merkle state design of accumulate allows anyone to prove the state of any account within accumulate without requiring direct access to the blockchain.  For example, an identity can be proven to be valid given a set of merkle states and an anchor to the directory node within.  Individual portions of the identity account can be anchored to other chains.  This will allow proofs to be delivered outside the protocol that can still be verifiable by third party tools and applications. The anchors can be published and secured on secondary chains, such as Bitcoin, Ethereum, Polygon, Fantom, etc.

Proving the proofs is relatively straightforward.  It involves generating the starting hash from the data contained within the state and computing the anchor from the merkle state. The data below is an example receipt for a given identity “acc://blockportal.”

Table 1: Receipt for an accumulate identity account


```
{
 "account": {
   "type": "identity",
   "keyBook": "acc://blockportal/book0",
   "url": "acc://blockportal",
   "authorities": [
     {
       "url": "acc://blockportal/book0"
     }
   ]
 },
 "chainState": [
   {
     "name": "main",
     "type": "transaction",
     "height": 1,
     "count": 1,
     "roots": [
       "bf006da33a2a899530471d4a0e88d8eca2c5f49c092828e1344223e3cf633867"
     ]
   },
   {
     "name": "minor-main-index",
     "type": "index",
     "height": 1,
     "count": 1,
     "roots": [
       "02e0012000000000000000000000000000000000000000000000000000000000"
     ]
   },
   {
     "name": "signature",
     "type": "transaction",
     "height": 2,
     "count": 2,
     "roots": [
       "cc360b6231d6b7eea042c70d8691ea6d00ffa590ab96a6f7edd2ad6c49968978",
       "b7d58d3a2d39e712c5e67a3057a66d4c894d5d0ff3913c2c47c3bf853b24398d"
     ]
   }
 ],
 "receipt": {
   "localBlock": 66,
   "receipt": {
     "start": "d2f09f8c9e1025d9df4940bd85d14eadb7c29a71461e50aadd05205359595f9c",
     "result": "6a3b55416de3556fb57e02fa717b78be967980a763a60870aa99888cc2f332b5",
     "entries": [
       {
         "right": true,
         "hash": "bf006da33a2a899530471d4a0e88d8eca2c5f49c092828e1344223e3cf633867"
       },
       {
         "right": true,
         "hash": "7b00c49b10228b020b87352d887a4c3fd3526f32581151dbb955c50aea1631c4"
       },
       {
         "hash": "9431be0a2dc0515619ee0e193f7fc745d056f22bbea92b0e592a6b4b8c52bf7e"
       },
       {
         "hash": "b50b13e256367c8abfc49c64f63465f90bc6e96841913ac5c305bc79d44607cb"
       },
       {
         "hash": "782fa06f37ea0c7df5c952abb5a7315a7370af6b1109474faab9b8eb75a99f8c"
       },
       {
         "right": true,
         "hash": "317ba12c3d07d5138d10fbc7cd6370737ba36a0c1dbfd3b14d4506ff4a2260b9"
       },
       {
         "right": true,
         "hash": "64e4eb549bdc925469f2e0a5c6fc7b99decadb93d546a76433f38c946445d5e6"
       },
       {
         "hash": "60fb0cda5b8ac1ec4a00c35c86a5ceb1629b4e9b10218fe3053633350e6b7c20"
       },
       {
         "hash": "795d2cd1b003f41ba26a8e95f38cce2cd3a28d82c4d07ab74bbb68350136ba37"
       },
       {
         "hash": "86b657c23f59b7136ca6b007a640656c9f4c9035346a53e9f3a4e5c5ce66a66e"
       },
       {
         "hash": "e9d40c2edd6231c8ebb3b5b3699eac9752525c45effd304a3fee9b6ef59b1deb"
       },
       {
         "hash": "c5830991e5e963231df3033dc6c7e09a207afc192db01e87df0da7be98e9889a"
       },
       {
         "hash": "2568434910f365a987dfacc8169c2c28a5966bb32f6796b7bede99393eefe59a"
       },
       {
         "right": true,
         "hash": "226a9b81c26864726080965b64895f1014cf02dfbfced604b50e35fc4577e61d"
       },
       {
         "hash": "8143047eeb3d728b3c63a58a7945cb59e25b42f2b0702d715b8ba72ed47add17"
       },
       {
         "right": true,
         "hash": "7eab7f8d62cfbf93a2b60d4dc03721af14dcb98ed2c22b83e4061e4468ae6a69"
       },
       {
         "hash": "8b5aa6747d40da624396bd0f5258bb8f6c62a085e1c1a81e4939a4c8c089845b"
       },
       {
         "hash": "42bad8f8c4a6eecebd675a45076d2509f1d25d25e19c4e246a1de167c9e0037d"
       }
     ]
   }
 }
}
```



## Verification of the proof

To verify the proof, the merkle state needs to be rebuilt to equal the anchor extracted from Table 1 and highlighted below in Table 2.

Table 2: Receipt anchor (“_receipt.result_”)


```

   …
   "receipt": {
     "start": "d2f09f8c9e1025d9df4940bd85d14eadb7c29a71461e50aadd05205359595f9c",
     "result": "6a3b55416de3556fb57e02fa717b78be967980a763a60870aa99888cc2f332b5",
     "entries": [
     …

```


The merkle state is assembled by a series of “nodes” which represent hashes building the tree.  To accomplish this a series of nodes need to be assembled as illustrated in Figure 1 using the data provided in the receipt from Table 1.

![drawing](images/merkle-state.png)
Figure 1: Merkle State

Figure 1 shows the creation of an anchor for 10 entries. The leftover hash 8+9 is combined with 1234567 to create 123456789 to form a Merkle Directed Acyclic Graph (DAG) root. This Merkle DAG root serves as the anchor for the proof. The Merkle DAG does provide the needed proof of inclusion and order and number of the elements, only the proof of entry doesn’t have the same number of levels of hashes for 0-7 as it does for 8-9.  This makes the computation of the anchor straightforward.



In Table 1, each of the _receipt.entries_ represents the starting node and with a flag that indicates if the node is right or left sided. By default the “_entry.right_” field is false, implying the node is a left node in the computation. The algorithm for validating the proof is shown in the pseudocode in Table 3. Note: the “|” indicates a binary concatenation of the two hashes into a 64 byte array prior to being hashed back into a 32 byte array. The “_start_” field represents the account state hash which will be discussed in the next section

Table 3: Pseudocode for computing the anchor from the receipt.


```
anchor := start // To begin with, we start with the object as the anchor

// Now apply all the path hashes to the root
for _, node := range receipt.Entries {
  if node.Right {
     // If this hash comes from the right, apply it that way
     anchor = sha256(anchor | node.Hash)
  } else {
     // If this hash comes from the left, apply it that way
     anchor = sha256(node.Hash | anchor)
  }
}

// In the end, the anchor should be the same hash the receipt expects.
return anchor == receipt.Result 

```



## Building the account state start hash

The example proof given in Table 1 above also contains the account state data needed to prove the account.  Therefore, to fully complete validation of the receipt, the _receipt.start_ hash must be reproduced (d2f09f8c9e1025d9df4940bd85d14eadb7c29a71461e50aadd05205359595f9c) using the source data . To do this, the account state data object needs to be reconstructed, marshaled, then hashed via sha256. The account state data is in the above receipt as the _account_ and is shown for convenience in Table 2.

Table 4: The account data for an identity


```
"account": {
   "type": "identity",
   "url": "acc://blockportal",
   "authorities": [
     {
       "url": "acc://blockportal/book0"
     }
   ]
 }
```


Accumulate uses custom marshalers to serialize the binary accounts. Unfortunately, marshaling of this binary is non-trivial, however, there are several language bindings that exist for various languages to help facilitate building the binary required for the hash computation of account state.  Reference code for the account is provided in the table below.

Table 5: Current and Planned Supported language bindings for Accumulate development


<table>
  <tr>
   <td>Language
   </td>
   <td>Repository
   </td>
  </tr>
  <tr>
   <td>Go
   </td>
   <td>https://gitlab.com/accumulatenetwork/accumulate/-/tree/develop/protocol
   </td>
  </tr>
  <tr>
   <td>Javascript
   </td>
   <td>https://gitlab.com/accumulatenetwork/accumulate.js/-/tree/main/src/types
   </td>
  </tr>
  <tr>
   <td>Dart
   </td>
   <td>?
   </td>
  </tr>
  <tr>
   <td>Python
   </td>
   <td>Under development
   </td>
  </tr>
  <tr>
   <td>C#
   </td>
   <td>Under development
   </td>
  </tr>
  <tr>
   <td>C
   </td>
   <td>Under development
   </td>
  </tr>
</table>



## Compact Receipt

For convenience the receipt has been reprocessed into a more compact and usable form, where the account state has been preprocessed into the marshaled binary.  Additionally, the left/right indicator has been processed into a bitfield that correlates to the entries.  If the bit is 0 it indicates the entry hash is a left sided node, if the bit is 1 it indicates the entry hash is a right sided node. Table 6 is an example of a reprocessed compact receipt.  The account data is provided for convenience that can be used as additional verification of the marshaling, but is not required to satisfy the proof.  To compute the proof, convert the “marshaledAccount” field into binary and perform a sha256 hash on the binary.  This value must equal the “start” field hash.  Then proceed with the proof as outlined in Table 3 above.

Table 6: compact receipt


```
{
 "account": {
   "type": "identity",
   "keyBook": "acc://blockportal/book0",
   "url": "acc://blockportal",
   "authorities": [
     {
       "url": "acc://blockportal/book0"
     }
   ]
 },
 "marshaledAccount": "010202116163633a2f2f626c6f636b706f7274616c031b011901176163633a2f2f626c6f636b706f7274616c2f626f6f6b30",
 "start": "d2f09f8c9e1025d9df4940bd85d14eadb7c29a71461e50aadd05205359595f9c",
 "anchor": "6a3b55416de3556fb57e02fa717b78be967980a763a60870aa99888cc2f332b5",
 "lrBitfield": 41059,
 "entries": [
   "bf006da33a2a899530471d4a0e88d8eca2c5f49c092828e1344223e3cf633867",
   "7b00c49b10228b020b87352d887a4c3fd3526f32581151dbb955c50aea1631c4",
   "9431be0a2dc0515619ee0e193f7fc745d056f22bbea92b0e592a6b4b8c52bf7e",
   "b50b13e256367c8abfc49c64f63465f90bc6e96841913ac5c305bc79d44607cb",
   "782fa06f37ea0c7df5c952abb5a7315a7370af6b1109474faab9b8eb75a99f8c",
   "317ba12c3d07d5138d10fbc7cd6370737ba36a0c1dbfd3b14d4506ff4a2260b9",
   "64e4eb549bdc925469f2e0a5c6fc7b99decadb93d546a76433f38c946445d5e6",
   "60fb0cda5b8ac1ec4a00c35c86a5ceb1629b4e9b10218fe3053633350e6b7c20",
   "795d2cd1b003f41ba26a8e95f38cce2cd3a28d82c4d07ab74bbb68350136ba37",
   "86b657c23f59b7136ca6b007a640656c9f4c9035346a53e9f3a4e5c5ce66a66e",
   "e9d40c2edd6231c8ebb3b5b3699eac9752525c45effd304a3fee9b6ef59b1deb",
   "c5830991e5e963231df3033dc6c7e09a207afc192db01e87df0da7be98e9889a",
   "2568434910f365a987dfacc8169c2c28a5966bb32f6796b7bede99393eefe59a",
   "226a9b81c26864726080965b64895f1014cf02dfbfced604b50e35fc4577e61d",
   "8143047eeb3d728b3c63a58a7945cb59e25b42f2b0702d715b8ba72ed47add17",
   "7eab7f8d62cfbf93a2b60d4dc03721af14dcb98ed2c22b83e4061e4468ae6a69",
   "8b5aa6747d40da624396bd0f5258bb8f6c62a085e1c1a81e4939a4c8c089845b",
   "42bad8f8c4a6eecebd675a45076d2509f1d25d25e19c4e246a1de167c9e0037d"
 ]
}

```



## Anchoring to a smart contract

The final step is to anchor the data (i.e. "_receipt.result_") by publishing it on a public chain so it can be accessed by whoever is attempting to validate the proof.   This can be done by a smart contract, metadata stored in a bitcoin transaction, or some other public means of publication. Table 7 shows an anchoring smart contract that holds both the anchor and a public function to validate a receipt. The “_prove_” function will test data passed to the smart contract against the anchor.  If the computed proof matches the anchor, then the data is valid, otherwise it is rejected.
Table 7: Sample anchoring Solidity Smart Contract


```
//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

contract AccumulateAnchor{

bytes32 private anchor;

constructor(bytes32 a) {
    anchor = a;
}

function getAnchor() public view returns(bytes32) {
    return anchor;
}


//prove will validate a receipt against the anchor in the smart contract.  It requires the marshaled binary of 
//the state data, the starting hash the hashed marshaledState should hash to, a left / right bitfield, and the
//merkle node hash entries.  The bits in the lrBitfield will be 1 if the entry is a right handed node, or 0 
//if the entry is a left handed node.
function prove(bytes memory marshaledState, bytes32 start, uint64 lrBitfield, bytes32[] memory entries) external view returns(bool) {
    // To begin with, we start with the object as the md root / anchor
    bytes32 root = sha256(marshaledState);

    //compare the start of the proof and kick out if the hashes are not equal
    if ( root != start || entries.length > 64 ) {
        return false;
    }

    // Now apply all the path hashes to the root
    for (uint256 i = 0; i < entries.length; i++ ) 
    {
        if (lrBitfield & (1 << i) != 0) {
            // If this hash comes from the right, apply it that way
            root = sha256(abi.encodePacked(root,entries[i]));
            continue;
        }
        // If this hash comes from the left, apply it that way
        root = sha256(abi.encodePacked(entries[i],root));
    }
    // The anchor should be equal to the computed proof
    return (getAnchor() == root);
}
}
```

This smart contract has been published on the Ethereum (Ropsten) testnet, the Fantom Testnet, and the Polygon Mumbai Testnet. 
Table 8, links to the smart contract address (*0x0a9b03695e91f9e3b55a0127dad1581d4d47ea3a*) on each of the respective networks.

| Blockchain Network       | Contract                                                                            | 
|--------------------------|-------------------------------------------------------------------------------------|
| Ethereum Ropsten         | https://ropsten.etherscan.io/address/0x0a9b03695e91f9e3b55a0127dad1581d4d47ea3a#code |
| Fantom Testnet           | https://testnet.ftmscan.com/address/0x0a9b03695e91f9e3b55a0127dad1581d4d47ea3a      |
| Polygon (Mumbai) Testnet | https://mumbai.polygonscan.com/address/0x0a9b03695e91f9e3b55a0127dad1581d4d47ea3a                                           |
