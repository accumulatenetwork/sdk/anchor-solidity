// SPDX-License-Identifier: GPL-3.0
        
pragma solidity >=0.4.22 <0.9.0;

// This import is automatically injected by Remix
import "remix_tests.sol"; 

// This import is required to use custom transaction context
// Although it may fail compilation in 'Solidity Compiler' plugin
// But it will work fine in 'Solidity Unit Testing' plugin
import "remix_accounts.sol";
import "../anchor.sol";
import "./helpers.sol";

// File name has to end with '_test.sol', this file can contain more than one testSuite contracts
contract testSuite {

    struct CompactReceipt {
        bytes marshaledBinary;
        bytes32 start;
        bytes32 anchor;
        uint64 lrBitfield;
        bytes32[] entries;
    }

    CompactReceipt testData;


    AccumulateAnchor ar;
    /// 'beforeAll' runs before all other tests
    /// More special functions are: 'beforeEach', 'beforeAll', 'afterEach' & 'afterAll'
    function beforeAll() public {
        // <instantiate contract>
        testData.marshaledBinary = helpers.hex2bytes("010202116163633a2f2f626c6f636b706f7274616c031b011901176163633a2f2f626c6f636b706f7274616c2f626f6f6b30");
        testData.start = 0xd2f09f8c9e1025d9df4940bd85d14eadb7c29a71461e50aadd05205359595f9c;
        testData.anchor = 0x6a3b55416de3556fb57e02fa717b78be967980a763a60870aa99888cc2f332b5;
        testData.lrBitfield = 41059;
        testData.entries.push(0xbf006da33a2a899530471d4a0e88d8eca2c5f49c092828e1344223e3cf633867);
        testData.entries.push(0x7b00c49b10228b020b87352d887a4c3fd3526f32581151dbb955c50aea1631c4);
        testData.entries.push(0x9431be0a2dc0515619ee0e193f7fc745d056f22bbea92b0e592a6b4b8c52bf7e);
        testData.entries.push(0xb50b13e256367c8abfc49c64f63465f90bc6e96841913ac5c305bc79d44607cb);
        testData.entries.push(0x782fa06f37ea0c7df5c952abb5a7315a7370af6b1109474faab9b8eb75a99f8c);
        testData.entries.push(0x317ba12c3d07d5138d10fbc7cd6370737ba36a0c1dbfd3b14d4506ff4a2260b9);
        testData.entries.push(0x64e4eb549bdc925469f2e0a5c6fc7b99decadb93d546a76433f38c946445d5e6);
        testData.entries.push(0x60fb0cda5b8ac1ec4a00c35c86a5ceb1629b4e9b10218fe3053633350e6b7c20);
        testData.entries.push(0x795d2cd1b003f41ba26a8e95f38cce2cd3a28d82c4d07ab74bbb68350136ba37);
        testData.entries.push(0x86b657c23f59b7136ca6b007a640656c9f4c9035346a53e9f3a4e5c5ce66a66e);
        testData.entries.push(0xe9d40c2edd6231c8ebb3b5b3699eac9752525c45effd304a3fee9b6ef59b1deb);
        testData.entries.push(0xc5830991e5e963231df3033dc6c7e09a207afc192db01e87df0da7be98e9889a);
        testData.entries.push(0x2568434910f365a987dfacc8169c2c28a5966bb32f6796b7bede99393eefe59a);
        testData.entries.push(0x226a9b81c26864726080965b64895f1014cf02dfbfced604b50e35fc4577e61d);
        testData.entries.push(0x8143047eeb3d728b3c63a58a7945cb59e25b42f2b0702d715b8ba72ed47add17);
        testData.entries.push(0x7eab7f8d62cfbf93a2b60d4dc03721af14dcb98ed2c22b83e4061e4468ae6a69);
        testData.entries.push(0x8b5aa6747d40da624396bd0f5258bb8f6c62a085e1c1a81e4939a4c8c089845b);
        testData.entries.push(0x42bad8f8c4a6eecebd675a45076d2509f1d25d25e19c4e246a1de167c9e0037d);
        ar = new AccumulateAnchor(testData.anchor);
        Assert.equal(testData.anchor, ar.getAnchor(), "anchors do not match");
    }

    function checkSuccess() public {
        // Use 'Assert' methods: https://remix-ide.readthedocs.io/en/latest/assert_library.html
        //Assert.ok(2 == 2, 'should be true');
        //Assert.greaterThan(uint(2), uint(1), "2 should be greater than to 1");
        //Assert.lesserThan(uint(2), uint(3), "2 should be lesser than to 3");
        Assert.equal(ar.getAnchor(), 
          testData.anchor, 
          "anchors are not equal");
    }

    function checkSuccess2() public view returns (bool) {
        // Use the return value (true or false) to test the contract
        return ar.prove(testData.marshaledBinary, testData.start, testData.lrBitfield, testData.entries);
    }
    
    function checkFailure() public {
        Assert.notEqual(uint(2), uint(1), "1 should not be equal to 1");
    }

    /// Custom Transaction Context: https://remix-ide.readthedocs.io/en/latest/unittesting.html#customization
    /// #sender: account-1
    /// #value: 100
    function checkSenderAndValue() public payable {
        // account index varies 0-9, value is in wei
        Assert.equal(msg.sender, TestsAccounts.getAccount(1), "Invalid sender");
        Assert.equal(msg.value, 100, "Invalid value");
    }

}

