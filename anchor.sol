//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

contract AccumulateAnchor{

    bytes32 private anchor;

    constructor(bytes32 a) {
        anchor = a;
    }

    function getAnchor() public view returns(bytes32) {
        return anchor;
    }


    //prove will validate a receipt against the anchor in the smart contract.  It requires the marshaled binary of
    //the state data, the starting hash the hashed marshaledState should hash to, a left / right bitfield, and the
    //merkle node hash entries.  The bits in the lrBitfield will be 1 if the entry is a right handed node, or 0
    //if the entry is a left handed node.
    function prove(bytes memory marshaledState, bytes32 start, uint64 lrBitfield, bytes32[] memory entries) external view returns(bool) {
        // To begin with, we start with the object as the md root / anchor
        bytes32 root = sha256(marshaledState);

        //compare the start of the proof and kick out if the hashes are not equal
        if ( root != start || entries.length > 64 ) {
            return false;
        }

        // Now apply all the path hashes to the root
        for (uint256 i = 0; i < entries.length; i++ )
        {
            if (lrBitfield & (1 << i) != 0) {
                // If this hash comes from the right, apply it that way
                root = sha256(abi.encodePacked(root,entries[i]));
                continue;
            }
            // If this hash comes from the left, apply it that way
            root = sha256(abi.encodePacked(entries[i],root));
        }
        // The anchor should be equal to the computed proof
        return (getAnchor() == root);
    }
}